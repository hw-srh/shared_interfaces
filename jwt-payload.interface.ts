export interface JwtPayload {
    sub: number; // subject of the jwt (e.g. user id)
    exp: number; // expiration time
    jti: string; // uuid to identify this JWT in redis
    context: {
        user: {
            email: string;
            username: string;
        },
        roles: string[];
    }
}