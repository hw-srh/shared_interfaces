import {CreateTimeTrackingDto} from "./create-time-tracking.dto";

export type ResponseTimeTrackingDto = CreateTimeTrackingDto & { id: string; }