export interface CreateTimeTrackingDto {
    email: string;
    customerDescription: string;
    customerName: string;
    taskName: string;
    taskDescription: string;
    scheduledStart?: Date;
    scheduledEnd: Date;
    scheduledBreakTime?: string; // use "HH:MM" format e.g 01:17 one hour and 17 minutes
    actualStart?: Date;
    actualEnd?: Date;
    actualBreakTime?: string; // use "HH:MM" format
    workProtocol?: string;
}