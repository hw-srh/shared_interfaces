import {CreateTimeTrackingDto} from "./create-time-tracking.dto";

export type UpdateTimeTrackingDto = Partial<CreateTimeTrackingDto> & { id: string };