import {ResponseTimeTrackingDto} from "./response-time-tracking.dto";

export type RawResponseTimeTrackingDto = ResponseTimeTrackingDto & {
    scheduledStart: string;
    scheduledEnd: string;
    actualStart: string;
    actualEnd?: string;
}