export interface FilterTimeTrackingWithoutTimeRangeDto {
    taskName?: string;
    customerName?: string;
    userId?: number;
}