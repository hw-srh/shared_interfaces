import {FilterTimeTrackingWithoutTimeRangeDto} from "./filter-time-tracking-without-time-range.dto";

export interface FilterTimeTrackingDtoWithTimeRange extends FilterTimeTrackingWithoutTimeRangeDto {
    timeRangeStart: Date,
    timeRangeEnd: Date,
}