export interface LoginDto {
    password: string;
    userNameOrEmail: string;
}