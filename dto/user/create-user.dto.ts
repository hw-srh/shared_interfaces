export interface CreateUserDto {
    firstname: string;
    lastname: string;
    email: string;
    username?: string;
    workHoursPerWeek?: number;
    roles?: string[];
}