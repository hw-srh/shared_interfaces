// do not extend CreateUserDto, the password property should not be part of the response!
export interface ResponseUserDto {
    id: number;
    firstname: string;
    lastname: string;
    email: string;
    username: string;
    workHoursPerWeek: number;
    roles: string[];
}