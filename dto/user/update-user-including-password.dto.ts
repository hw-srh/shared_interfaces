import {CreateUserDto} from "./create-user.dto";

export interface UpdateUserIncludingPasswordDto extends Partial<CreateUserDto> {
    id: number;
    password: string;
    newPassword: string;
}