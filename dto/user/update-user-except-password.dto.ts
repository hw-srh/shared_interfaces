import {CreateUserDto} from "./create-user.dto";

export interface UpdateUserExceptPasswordDto extends Partial<CreateUserDto> {
    id: number;
}