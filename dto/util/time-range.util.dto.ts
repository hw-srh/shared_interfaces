export interface TimeRangeDto {
    start: Date,
    end: Date
}