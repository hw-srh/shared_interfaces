export interface PasswordResetDto {
    email: string;
    password: string;
    token: string;
};